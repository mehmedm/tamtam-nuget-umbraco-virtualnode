﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

namespace TamTam.RnD.Website.NuGet.Umb.VirtualNode.Identifiers
{
    public class VirtualNodeIdentifierByDocumentType : IVirtualNodeFinderComponent
    {
        /// <summary>
        /// This identifier is inactive by default. Change the <see cref="IsActive"/> to TRUE in order for this identifier to work
        /// </summary>
        public bool IsActive { get { return false; } }

        /// <summary>
        /// This identifier has a priority value of 2
        /// </summary>
        public int PriorityFactor { get { return 2; } }

        public bool IsVirtualNode(IPublishedContent node)
        {
            //see https://our.umbraco.org/documentation/reference/management/models/content#contenttypeid
            return (node != null && node.DocumentTypeAlias == "[DOCUMENT TYPE ALIAS HERE]");
        }

        /// <summary>
        /// Checks is the specified node is virtual by checking if it is of certain type
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public bool IsVirtualNode(IContentBase node)
        {
            //see https://our.umbraco.org/documentation/reference/management/models/content#contenttypeid
            return (node != null && node.ContentTypeId == 1234);
        }
    }
}