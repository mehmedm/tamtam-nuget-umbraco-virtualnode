﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TamTam.NuGet.Umb.VirtualNode;
using Umbraco.Core.Models;

namespace TamTam.RnD.Website.NuGet.Umb.VirtualNode
{
    /// <summary>
    /// Contains logic for identifying a virtual node
    /// </summary>
    public static class VirtualNodeIdenfitier
    {
        //PRIVATE FIELDS
        private static List<IVirtualNodeFinderComponent> _virtualNodeIdentifiers = null;



        //PUBLIC METHODS
        /// <summary>
        /// Uses all available implementations of <see cref="IVirtualNodeFinderComponent.IsVirtualNode(IContentBase)"/> to check if this node is virtual
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public static bool IsVirtual(IContentBase content)
        {
            bool isVirtual = false;

            var identifiers = VirtualNodeIdentifiers
                .Where(x => x.IsActive)
                .OrderByDescending(x => x.PriorityFactor);

            //pass the content node through a gauntlet of IVirtualNodeFinderComponent implementations
            foreach (IVirtualNodeFinderComponent identifier in identifiers)
            {
                if (identifier.IsVirtualNode(content))
                {
                    isVirtual = true;
                    break;
                }
            }

            return isVirtual;
        }

        /// <summary>
        /// Uses all available implementations of <see cref="IVirtualNodeFinderComponent.IsVirtualNode(IContentBase)"/> to check if this node is virtual
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public static bool IsVirtual(IPublishedContent content)
        {
            bool isVirtual = false;

            var identifiers = VirtualNodeIdentifiers
                .Where(x => x.IsActive)
                .OrderByDescending(x => x.PriorityFactor);

            //pass the content node through a gauntlet of IVirtualNodeFinderComponent implementations
            foreach (IVirtualNodeFinderComponent identifier in identifiers)
            {
                if (identifier.IsVirtualNode(content))
                {
                    isVirtual = true;
                    break;
                }
            }

            return isVirtual;
        }


        /// <summary>
        /// Gets all implementations of <see cref="IVirtualNodeFinderComponent"/> interface
        /// </summary>
        public static List<IVirtualNodeFinderComponent> VirtualNodeIdentifiers
        {
            get
            {
                //get the nodes if they are not identified yet
                if (_virtualNodeIdentifiers == null)
                {
                    _virtualNodeIdentifiers = GetAllIdentifiers();
                }

                return _virtualNodeIdentifiers;
            }
        }



        //PRIVATE METHODS
        /// <summary>
        /// Finds all implementations of <see cref="IVirtualNodeFinderComponent"/>,
        /// makes an instance for every implementation and returns it
        /// </summary>
        /// <returns></returns>
        private static List<IVirtualNodeFinderComponent> GetAllIdentifiers()
        {
            //get all implementations of IVirtualNodeIdentifier interface from the assembly where the NuGet package is installed
            Type[] implementations = typeof(VirtualNodeConstants).Assembly
                .GetTypes()
                .Where(t => t.IsClass
                    && !t.IsAbstract //cannot instantiate abstract classes
                    && !t.IsInterface //we don't want to get the interface, just the implementations
                    && t.GetInterfaces().Contains(typeof(IVirtualNodeFinderComponent)))
                .ToArray();

            //make an instance out of every implementation that was found, if any are available
            List<IVirtualNodeFinderComponent> instances = new List<IVirtualNodeFinderComponent>();
            if (implementations != null && implementations.Any())
            {
                instances = implementations.Select(t => (IVirtualNodeFinderComponent)Activator.CreateInstance(t))
                    .ToList();
            }

            //return the instances
            return instances;
        }
    }
}