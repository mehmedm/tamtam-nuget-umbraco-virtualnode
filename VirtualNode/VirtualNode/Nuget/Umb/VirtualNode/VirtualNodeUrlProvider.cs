﻿using System;
using System.Collections.Generic;
using Umbraco.Web;
using Umbraco.Web.Routing;

namespace TamTam.NuGet.Umb.VirtualNode
{
    public class VirtualNodeUrlProvider : IUrlProvider
    {
        readonly IUrlProvider defaultUrlProvider = new DefaultUrlProvider();

        public IEnumerable<string> GetOtherUrls(UmbracoContext umbracoContext, int id, Uri current)
        {
            var allUrls =  defaultUrlProvider.GetOtherUrls(umbracoContext, id, current);
            return allUrls;
        }

        public string GetUrl(UmbracoContext umbracoContext, int id, Uri current, UrlProviderMode mode)
        {
            string url = defaultUrlProvider.GetUrl(umbracoContext, id, current, mode);

            //check if this piece of content is a child of a virtual node
            if(!string.IsNullOrWhiteSpace(url) && url.Contains(VirtualNodeConstants.UrlSegmentMarker))
            {
                //remove the virtual node markers from the URL
                string partToRemove = VirtualNodeConstants.UrlSegmentMarker + "/";
                url = url.Replace(partToRemove, string.Empty);
            }
           
            return url;
        }
    }
}