﻿using System;
using TamTam.NuGet.Umb.VirtualNode;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace TamTam.RnD.Website.NuGet.Umb.VirtualNode.Identifiers
{
    /// <summary>
    /// Contains logic that can identify a virtual node by it's properties
    /// </summary>
    public class VirtualNodeIdentifierByProperty : IVirtualNodeFinderComponent
    {
        public bool IsActive { get { return true; } }

        /// <summary>
        /// This identifer has a priority value of 1
        /// </summary>
        public int PriorityFactor
        {
            get
            {
                return 1;
            }
        }

        /// <summary>
        /// Identifies if a node is virtual by check it's properties. 
        /// It uses '<see cref="VirtualNodeConstants.PropertyName"/>' to match the name of the property
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public bool IsVirtualNode(IPublishedContent node)
        {
            bool isVirtual = false;

            if (node.HasProperty(VirtualNodeConstants.PropertyName))
            {
                IPublishedProperty isVirtualProperty = node.GetProperty(VirtualNodeConstants.PropertyName);

                try
                {
                    isVirtual = (bool)isVirtualProperty.Value;
                }
                catch
                {
                    isVirtual = false;
                }
                
            }

            return isVirtual;
        }

        /// <summary>
        /// Identifies if a node is virtual by check it's properties. 
        /// It uses '<see cref="VirtualNodeConstants.PropertyName"/>' to match the name of the property
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public bool IsVirtualNode(IContentBase node)
        {
            bool isVirtual = false;

            if (node.HasProperty(VirtualNodeConstants.PropertyName))
            {
                var isVirtualProperty = node.Properties[VirtualNodeConstants.PropertyName];
                isVirtual = (int)isVirtualProperty.Value == 1;
            }

            return isVirtual;
        }
    }
}