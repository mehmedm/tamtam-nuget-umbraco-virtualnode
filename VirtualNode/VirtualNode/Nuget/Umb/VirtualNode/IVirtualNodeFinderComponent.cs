﻿using Umbraco.Core.Models;

namespace TamTam.RnD.Website.NuGet.Umb.VirtualNode
{
    /// <summary>
    /// Contains logic that can identify a virtual node
    /// </summary>
    public interface IVirtualNodeFinderComponent
    {
        /// <summary>
        /// If true, this Virtual Node finder component will be used to identify potential virtual nodes, otherwise it will be skiped
        /// </summary>
        bool IsActive { get; }

        /// <summary>
        /// Priority factor of the identifier. Larger numbers mean more priority when content nodes are being identified
        /// </summary>
        int PriorityFactor { get; }

        /// <summary>
        /// Identifies a virtual node from a base content node. Returns true if the provided content is a virtual node and false otherwise
        /// </summary>
        /// <param name="node">Base node that you want to identify as virtual</param>
        /// <returns></returns>
        bool IsVirtualNode(IContentBase node);

        /// <summary>
        /// Identifies a virtual node from an <see cref="IPublishedContent"/> node. Returns true if provided content is a virtual node and false otherwise 
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        bool IsVirtualNode(IPublishedContent node);
    }
}
