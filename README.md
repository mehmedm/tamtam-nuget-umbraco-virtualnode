# Virtual Node package#

Virtual Node pacakge allows the users to mark an Umbraco node as 'virtual', which will in turn ensure that the virtualized node no longer affects the URL.

### Example ###

We have the following tree:

* Friends
* * Europe
* * * Tom 
* * * Marcy
* * * Simon
* * North America
* * * Roy
* * * Bob
* * * Davion

If we wanted to access Tom for example, we would type in **/friends/europe/tom** in the address bar.
However, in case we do not want the **/europe/** part, this package allows it by letting the user mark the europe node as virtual.

That would result in turning the original URL for accessing Tom in to **/friends/tom**

## How does it work? ##

### Structure ###

After installing the package you will get the following file structure in your project:

![VirtualNode_Tree.png](https://bitbucket.org/repo/neRd6a/images/1685609548-VirtualNode_Tree.png)

### Virtual node identifiers ###
Classes in the Identifier folder are just as their name suggests - they are implementations of the **IVirtualNodeFinderComponent** interface and their purpose is to identify what a virtual node is. You can use the default 2 default ones that come with the package or implement your own that will suit what ever specialized situation you may have going on in the project. 

The interface is simple to implement"


```
#!C#

    /// <summary>
    /// Contains logic that can identify a virtual node
    /// </summary>
    public interface IVirtualNodeFinderComponent
    {
        /// <summary>
        /// If true, this Virtual Node finder component will be used to identify potential virtual nodes, otherwise it will be skiped
        /// </summary>
        bool IsActive { get; }

        /// <summary>
        /// Priority factor of the identifier. Larger numbers mean more priority when content nodes are being identified
        /// </summary>
        int PriorityFactor { get; }

        /// <summary>
        /// Identifies a virtual node from a base content node. Returns true if the provided content is a virtual node and false otherwise
        /// </summary>
        /// <param name="node">Base node that you want to identify as virtual</param>
        /// <returns></returns>
        bool IsVirtualNode(IContentBase node);

        /// <summary>
        /// Identifies a virtual node from an <see cref="IPublishedContent"/> node. Returns true if provided content is a virtual node and false otherwise 
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        bool IsVirtualNode(IPublishedContent node);
    }
```

**IsActive** is a read-only property where you determine if the identifier will be used during project run-time. By setting it to false you can ensure that the identifier is not used thus allowing easy debugging of other identifiers, should the need arise. 

**PriorityFactor** is an integer that determines the order in which the identifier will be used during runtime. Larger the number is, sooner the identifier will be used/prioritized. 


**IsVirtualNode()** methods are the important part. These methods recieve an IContentBase or IPublishedContent. Your job is to determine if it is a going to be considered as virtual or not. Return *true* if you think the node that was plugged in is virtual or false otherwise.

Version with *IContentBase* is used during content publishing and *IPublishedContent* when the content is being searched for.


###VirtualNodeContentFinder##
Probably the single most important class in the package.
It is used to actually find the content once Umbraco fails to find the content using the altered URL (where one or more nodes are missing in the URL chain).
Job of this class is to pick up where original content finder fails.

It works in a depth-first search. It will search from the very root and follow the URL segments, one by one until it reaches a part where a node cannot be found. After that the code will start looking for the virtual nodes that are invisible in the URL and start looking for the content within them.

##**IMPORTANT**##

Method **IsHomepage** is very important in adapting the package to your project. Depending on your project structure you may have more than one website or more homepages. 
Since the search starts at the homepage and drills down from there you must adapt this method to recognize what a homepage is in your project. If you have a more unique project structure, **adapt this method**
```
#!C#

        /// <summary>
        /// Checks if the content page is actualy a homepage.
        /// This method is required when the TryFindContent method tries to find a starting point for the search.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private bool IsHomepage(IPublishedContent node)
        {
            string documentTypeAlias = node.DocumentTypeAlias.ToLower();
            return documentTypeAlias == "home" || documentTypeAlias == "homepage";
        }
```

Also, if you have a unique project structure you may need to adapt this code as well:


```
#!C#

        public bool TryFindContent(PublishedContentRequest contentRequest)
        {
            if (contentRequest == null)
                return false;

            //get the current url
            var url = contentRequest.Uri.AbsolutePath;

            try
            {   
                //chop up the url
                var urlParts = url.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                if (urlParts != null && urlParts.Any())
                {
                    int currentRouteDepth = 0;
                    IPublishedContent previousNode = null;
                    IPublishedContent currentNode = null;
                    
                    DefaultUrlSegmentProvider defaultSegmentProvider = new DefaultUrlSegmentProvider();
                    
                    //get all root nodes
                    var rootNodes = contentRequest.RoutingContext.UmbracoContext.ContentCache.GetAtRoot()
                        .Where(node => node.GetCulture().Name == contentRequest.Culture.Name)
                        .ToList();
                    
                    if(rootNodes == null || !rootNodes.Any())
                    {
                        //if there are no websites with matching culture just get them all
                         rootNodes = contentRequest.RoutingContext.UmbracoContext.ContentCache.GetAtRoot()
                        .ToList();
                    }

                    foreach (var nextRootNode in rootNodes)
                    {
                        bool scanDeeper = true;

                        //get the initial part/content node of the URL
                        string homePageRoute = urlParts.First();
                        IPublishedContent root = nextRootNode.Children
                                .FirstOrDefault(childNode => childNode.UrlName == homePageRoute);

                        if (root == null)
                        {
                            //find the starting point of our search which should be the homepage
                            root = nextRootNode.DescendantsOrSelf().FirstOrDefault(node => IsHomepage(node));
                        }

                       **{...}**

```

That code snippet is looking for an entry point in your website structure to start looking for content. If the package does not find the content properly, the issue is most likely because it fails to find a proper entry point.

### Who do I talk to? ###

* Mehmed Maloparic (mehmedm@tamtam.nl)