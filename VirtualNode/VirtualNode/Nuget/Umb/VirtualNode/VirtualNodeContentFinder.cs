﻿using System;
using System.Linq;
using TamTam.RnD.Website.NuGet.Umb.VirtualNode;
using Umbraco.Core.Models;
using Umbraco.Core.Strings;
using Umbraco.Web;
using Umbraco.Web.Routing;

namespace TamTam.NuGet.Umb.VirtualNode
{
    public class VirtualNodeContentFinder : IContentFinder
    {
        /// <summary>
        /// Checks if the content page is actualy a homepage.
        /// This method is required when the TryFindContent method tries to find a starting point for the search.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private bool IsHomepage(IPublishedContent node)
        {
            string documentTypeAlias = node.DocumentTypeAlias.ToLower();
            return documentTypeAlias == "home" || documentTypeAlias == "homepage";
        }


        public bool TryFindContent(PublishedContentRequest contentRequest)
        {
            if (contentRequest == null)
                return false;

            //get the current url
            var url = contentRequest.Uri.AbsolutePath;

            try
            {   
                //chop up the url
                var urlParts = url.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                if (urlParts != null && urlParts.Any())
                {
                    int currentRouteDepth = 0;
                    IPublishedContent previousNode = null;
                    IPublishedContent currentNode = null;
                    
                    DefaultUrlSegmentProvider defaultSegmentProvider = new DefaultUrlSegmentProvider();
                    
                    //get all root nodes
                    var rootNodes = contentRequest.RoutingContext.UmbracoContext.ContentCache.GetAtRoot()
                        .Where(node => node.GetCulture().Name == contentRequest.Culture.Name)
                        .ToList();
                    
                    if(rootNodes == null || !rootNodes.Any())
                    {
                        //if there are no websites with matching culture just get them all
                         rootNodes = contentRequest.RoutingContext.UmbracoContext.ContentCache.GetAtRoot()
                        .ToList();
                    }

                    foreach (var nextRootNode in rootNodes)
                    {
                        bool scanDeeper = true;

                        //get the initial part/content node of the URL
                        string homePageRoute = urlParts.First();
                        IPublishedContent root = nextRootNode.Children
                                .FirstOrDefault(childNode => childNode.UrlName == homePageRoute);

                        if (root == null)
                        {
                            //find the starting point of our search which should be the homepage
                            root = nextRootNode.DescendantsOrSelf().FirstOrDefault(node => IsHomepage(node));
                        }

                        if (root != null)
                        {
                            currentNode = root;
                            currentRouteDepth = currentRouteDepth + 1 > urlParts.Length
                                ? currentRouteDepth + 1
                                : currentRouteDepth;                            

                            while (scanDeeper)
                            {
                                //got the route, now go deeper down the route until we hit a null - virtual node may be there!
                                previousNode = currentNode;
                                currentNode = previousNode.Children.FirstOrDefault(child => child.UrlName.ToLower() == urlParts[currentRouteDepth]);
                                if (currentNode == null)
                                {
                                    //no child? could be that we have a virtual node here - get them all
                                    var virtualNodes = previousNode.Children.Where(child => VirtualNodeIdenfitier.IsVirtual(child));

                                    //go in to every virtual node and perform an in-depth search for the file
                                    foreach (var virtualNode in virtualNodes)
                                    {
                                        IPublishedContent targetedContent = SearchVirtualNodeInDepth(virtualNode, urlParts, currentRouteDepth);
                                        if (targetedContent != null)
                                        {                                            
                                            contentRequest.PublishedContent = targetedContent;
                                            return true;
                                        }
                                    }

                                    //no more virtual nodes found and content did not turn up before it, 
                                    //so stop the search as content is not there
                                    scanDeeper = false;
                                }
                                else
                                {
                                    //regular node, continue deeper
                                    currentRouteDepth++;
                                    scanDeeper = (currentRouteDepth <= urlParts.Length);
                                }
                            }
                        }
                    }


                    
                }

                return false;
            }
            catch(Exception ex)
            {
                return false;
            }
        }


        /// <summary>
        /// Performs a recursive in-depth search of a virtual node and any child virtual nodes for the content that matches the route specified
        /// </summary>
        /// <param name="originalVirtualNode">Virtual node that you are searching through for content</param>
        /// <param name="urlParts">All the URL parts that specify which content to find and on which path</param>
        /// <param name="currentRouteDepth">Index that specifies how far down the path specified in the 'urlParts' parameter we got in our search</param>
        /// <returns></returns>
        private IPublishedContent SearchVirtualNodeInDepth(IPublishedContent originalVirtualNode, string[] urlParts, int currentRouteDepth)
        {
            IPublishedContent previousNode = originalVirtualNode;
            IPublishedContent currentNode = originalVirtualNode;
            bool scanDeeper = true;

            if (originalVirtualNode != null)
            {
                //we got the virtual node - check if it contains the file we are looking for
                while (scanDeeper)
                {
                    //got the route, now go deeper down the route until we hit a null - virtual node may be there!
                    previousNode = currentNode;
                    currentNode = previousNode.Children.FirstOrDefault(child => child.UrlName.ToLower() == urlParts[currentRouteDepth]);
                    if (currentNode == null)
                    {
                        //no child? could be that we have ANOTHER virtual node here? if so, get them all
                        var virtualNodes = previousNode.Children.Where(child => VirtualNodeIdenfitier.IsVirtual(child));

                        //go in to every virtual node and perform an in-depth search for the file
                        foreach (var virtualNode in virtualNodes)
                        {
                            IPublishedContent targetedContent = SearchVirtualNodeInDepth(virtualNode, urlParts, currentRouteDepth);
                            if (targetedContent != null)
                            {
                                //found the content - return it to the calling function so it can finalize the algorithm
                                return targetedContent;
                            }
                        }

                        //no more child virtual nodes found and content did not turn up before it, 
                        //so stop the search as content is not in this specific virtual node
                        scanDeeper = false;
                    }
                    else
                    {
                        //is this the last item in the urlParts array? if so, this is our content! 
                        if (currentRouteDepth == urlParts.Length - 1)
                        {
                            return currentNode;
                        }
                        else
                        {
                            //regular node, continue deeper
                            currentRouteDepth++;
                            scanDeeper = true;
                        }
                    }
                }

                //nothing was found in this virtual node or it's children - return null so algorithm can continue or finish
                return null;
            }
            else return null;
        }
    }
}