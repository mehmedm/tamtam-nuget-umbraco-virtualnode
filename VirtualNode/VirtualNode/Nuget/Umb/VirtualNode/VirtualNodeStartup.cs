﻿using Umbraco.Core;
using Umbraco.Core.Strings;
using Umbraco.Web.Routing;

namespace TamTam.NuGet.Umb.VirtualNode
{
    public class VirtualNodeStartup : IApplicationEventHandler
    {
        public void OnApplicationInitialized(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            //do nothing
        }

        public void OnApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            //do nothing
        }

        public void OnApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            UrlProviderResolver.Current.InsertType<VirtualNodeUrlProvider>();
            UrlSegmentProviderResolver.Current.InsertType<VirtualNodeUrlSegmentProvider>();
            ContentFinderResolver.Current.InsertType<VirtualNodeContentFinder>();
        }
    }
}