﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using TamTam.RnD.Website.NuGet.Umb.VirtualNode;

namespace TamTam.NuGet.Umb.VirtualNode
{
    /// <summary>
    /// Contains the constants for the Virtual Node plugin
    /// </summary>
    public static class VirtualNodeConstants
    {
        /// <summary>
        /// Gets the marker for a URL segment that signalizes a Virtual Node.
        /// </summary>
        public static string UrlSegmentMarker { get { return "tamtam-virtual-node-marker"; } }

        /// <summary>
        /// Gets the name of the data-cache used to store any content nodes that are children of virtual nodes
        /// </summary>
        public static string CacheName { get { return "CachedVirtualNodeContent"; } }

        /// <summary>
        /// Gets the name of the boolean property that determines if a content node is a virtual node
        /// </summary>
        public static string PropertyName { get { return "isVirtual"; } }  
    }
}