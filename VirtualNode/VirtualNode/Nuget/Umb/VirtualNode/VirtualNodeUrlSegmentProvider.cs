﻿using System.Globalization;
using System.Linq;
using TamTam.RnD.Website.NuGet.Umb.VirtualNode;
using Umbraco.Core.Models;
using Umbraco.Core.Strings;

namespace TamTam.NuGet.Umb.VirtualNode
{
    public class VirtualNodeUrlSegmentProvider : IUrlSegmentProvider
    {
        private readonly IUrlSegmentProvider defaultSegmentProvider = new DefaultUrlSegmentProvider();

        /// <summary>
        /// Returns a URL segment that will represent a specific content node in the URL
        /// </summary>
        /// <param name="content">Content node for which you want to generate a URL segment</param>
        /// <returns></returns>
        public string GetUrlSegment(IContentBase content)
        {
            bool isVirtual = VirtualNodeIdenfitier.IsVirtual(content);

            if (isVirtual)
            {
                return VirtualNodeConstants.UrlSegmentMarker;
            }
            else
            {
                return defaultSegmentProvider.GetUrlSegment(content);
            }
        }

        /// <summary>
        /// Returns a URL segment that will represent a specific content node of specified culture in the URL
        /// </summary>
        /// <param name="content">Content node for which you want to generate a URL segment</param>
        /// <param name="culture">Culture version of the content node</param>
        /// <returns></returns>
        public string GetUrlSegment(IContentBase content, CultureInfo culture)
        {            
            if (VirtualNodeIdenfitier.IsVirtual(content))
            {
                return this.GetUrlSegment(content);
            }
            else return defaultSegmentProvider.GetUrlSegment(content, culture);
        }
    }
}